package com.example.myfirstapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionService {

    private final AppCompatActivity app;


    public PermissionService(AppCompatActivity app) {
        this.app = app;
    }

    public static boolean isPersmissionGranted(final AppCompatActivity app, final String permission){
        return ContextCompat.checkSelfPermission(app, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public boolean requestPermission(final String permission, final int REQUEST_PERMISSION) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(app,
                permission)) {
            showRequestPermissionDialog(app, permission, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    ActivityCompat.requestPermissions(app, new String[]{permission}, REQUEST_PERMISSION);
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    showPermissionSetting(app);

                }
            }, false);
        } else {
            ActivityCompat.requestPermissions(app,
                    new String[]{permission},
                    REQUEST_PERMISSION);

        }
        return false;
    }

    public static AlertDialog showRequestPermissionDialog(AppCompatActivity app, String permission, DialogInterface.OnClickListener okOnClick, DialogInterface.OnClickListener noOnClick, boolean cancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(app);
        builder.setTitle("Request permission for " + permission);
        builder.setCancelable(cancelable);
        builder.setMessage("R string request permission");
        builder.setPositiveButton(android.R.string.ok, okOnClick);
        builder.setNegativeButton(android.R.string.no, noOnClick);

        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    public static void showPermissionSetting(AppCompatActivity app) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", app.getPackageName(), null);
        intent.setData(uri);
        app.startActivity(intent);
    }

    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            List<String> deniedPermissions = new ArrayList<>();
            for (int index = 0; index < grantResults.length; ++index) {
                if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                    deniedPermissions.add(permissions[index]);
                }
            }
            if (deniedPermissions.size() > 0) {
                Toast.makeText(app, "ERROR: permissions for " + String.join(", ", deniedPermissions) + " are not granted", Toast.LENGTH_LONG).show();
                showPermissionSetting(app);
                return false;
            }
            return true;
    }
}