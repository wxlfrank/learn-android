package com.example.myfirstapp;

import android.Manifest;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    public final PermissionService permissionService = new PermissionService(this);
    private TextureView mTextureView;
    private CameraService cameraService;
    private static final int REQUEST_CAMERA_PERMISSION = 0;
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            cameraService.openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            cameraService.configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextureView = findViewById(R.id.textureView);
        new LogService(this, (EditText) findViewById(R.id.log));
        cameraService = new CameraService(this, mTextureView);
        Button takePictureButton = findViewById(R.id.button);
        assert takePictureButton != null;
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        LogService.log("create");
        LogService.log(this.getApplicationContext().getExternalFilesDir("/").getAbsolutePath());
        LogService.log(Environment.getExternalStorageDirectory().getAbsolutePath());

        startApp();
    }

    private void startApp() {
        if ((this.cameraService.getCamera() == null) && PermissionService.isPersmissionGranted(this, Manifest.permission.CAMERA)) {
            this.cameraService.startBackgroundThread();


            // When the screen is turned off and turned back on, the SurfaceTexture is already
            // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
            // a camera and start preview from here (otherwise, we wait until the surface is ready in
            // the SurfaceTextureListener).
            if (mTextureView.isAvailable()) {
                this.cameraService.openCamera(mTextureView.getWidth(), mTextureView.getHeight());
            } else {
                mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
            }
        } else {
            this.permissionService.requestPermission(Manifest.permission.CAMERA, REQUEST_CAMERA_PERMISSION);
        }
    }

    private void takePicture() {
        if (PermissionService.isPersmissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            this.cameraService.takePicture();
        } else {
            this.permissionService.requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_STORAGE_PERMISSION);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.startApp();
    }

    @Override
    public void onPause() {
        cameraService.closeCamera();
        cameraService.stopBackgroundThread();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissionService.onRequestPermissionsResult(requestCode, permissions, grantResults))
                    this.startApp();
                break;
            case REQUEST_STORAGE_PERMISSION:
                if (permissionService.onRequestPermissionsResult(requestCode, permissions, grantResults))
                    this.takePicture();
                break;

        }
    }


}
