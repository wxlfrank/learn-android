package com.example.myfirstapp;

import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class LogService {
    public static LogService logService = null;

    AppCompatActivity app;
    private static EditText log;
    public LogService(AppCompatActivity app, EditText log) {
        this.app = app;
        this.log = log;
        logService = this;
    }

    public static void log(String message){
        if(logService != null){
            log.append(message + System.lineSeparator());
        }
        
    }
}
